{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
  };
  outputs = { self, nixpkgs, ... }:
    let
      supportedSystems = [ "x86_64-linux" "aarch64-linux" ];
      forEachSupportedSystem = f:
        nixpkgs.lib.genAttrs supportedSystems (system:
          f {
            pkgs = import nixpkgs { inherit system; };
          });
    in
    {
      devShells = forEachSupportedSystem ({ pkgs }: {
        default =
          let
            texpkgs = pkgs.texlive.combine {
              inherit (pkgs.texlive)
                scheme-basic collection-xetex collection-pictures
                collection-latexrecommended collection-latexextra
                collection-fontsextra;
            };
          in
          pkgs.mkShell {
            packages = with pkgs; [
              tectonic
            ];
            buildInputs = [
              texpkgs
            ];
          };
      });
    };
}
